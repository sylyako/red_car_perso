
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include "noeud.hpp"

using namespace std;

//const int 6 = 6;


Noeud::Noeud(int _plateau[],Noeud * _pere){

  this->plateau=_plateau;
  // for(int i=0 ; i<6 ; i++){
  //   for(int j=0 ; j<6 ; j++){
  //     this->plateau[i][j]=_plateau[i][j];
  //   }
  // }

  this->pere = _pere;
}
//
bool Noeud::comparePlateau(int coupDejaVisite[]){

  for(int i=0; i<36 ; i++){
    for (int j=0; j<6;j++){
      if( this->plateau[i+j]!=coupDejaVisite[i+j]){
        return false;
      }
    }
  }
  return true;


}
//
void Noeud::addFils(Noeud * Newfils){

    this->fils.push_back(Newfils);

}
//
// int * [][6] Noeud::recopiePlateau(){
//   return this->plateau;
// }
//
bool Noeud::isFree(int i,int j){

    if( this->plateau[i*6+j]==0 ){
      return true;
    }
    else{
      return false;
    }
}

bool Noeud::isGagnant(){
  if (this->plateau[2*6+5]){
    return true;
  }
  else{
    return false;
  }

}

// void Noeud::delete(){
//
// }
