#ifndef NOEUD
#define NOEUD

using namespace std;
//extern const int 6;

class Noeud {

  public :

  int* plateau;

  Noeud * pere ;
  vector<Noeud *>fils;

  bool comparePlateau(int plateau[]);

  void addFils(Noeud *);

  // int * recopiePlateau();
  //
  bool isFree(int i,int j);

  Noeud(int plateau[],Noeud* pere);

  bool isGagnant();

};

#endif
