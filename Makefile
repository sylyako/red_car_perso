OPTION=-g -Wall -std=c++11

all : exec

noeud.o : noeud.cpp noeud.hpp
	g++ $(OPTION) -c noeud.cpp -o noeud.o

main.o : main.cpp noeud.hpp
	g++ $(OPTION) -c main.cpp -o main.o

exec : main.o noeud.o
	g++ $(OPTION) main.o noeud.o -o exec

clean :
	rm -f *.o exec
